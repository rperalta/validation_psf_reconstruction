#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
=================
psf_analysis.py
=================

PSF Analysis tools:
- Airy pattern fit
- Ellipticity measurement
- Circled energy r68 & r95

Compute ellipticity information on the given image from the weighted
second moments, using an iterative scheme to find the image centroid

Usage:
  mk_ellipticity.py <fitsfile>
                    [-s, --sigma] [-p, --pix] [-n]
                    [-h, --help]

Args:
  fitsfile              input image in FITS format

Optionals:
  -h, --help            show this help message and exit
  -p, --pix             pixel scale [arcsec] of the input image
                        (default 1.0 arcsec/pix)
  -s, --sigma           width of the weighting gaussian distribution in arcsec
                        (default 0.75 arcsec)
  -n                    number of weighting iterations for the centroid
                        (default 4)

Example:
  mk_ellipticity.py psf.fits -p 0.01 -n 3



Version:
  1.0

Author & date:
  R. PERALTA - Sept 2019 

"""

import sys
import os
import glob
import astropy.io.fits as pyfits
import numpy as np
import matplotlib.pyplot as plt
import mk_ellipticity 
import psf_fitting
import math
import argparse
import sys


def get_data_and_header(fits_name):

    """
    Return the image and its header from a fits file
    Input: fits file
    Output: data and its header as a tuple (header,image)

    """
    data, header = pyfits.getdata(fits_name,header=True)
    return (data, header)


def get_psf_position(fits_file):

    """
    Return the psf positions
    Input: fits file
    Output: the psf position as a tuple (col, row) [or (x, y)]

    """
    data, header = get_data_and_header(fits_file)
    col_position = header['XFIELD']
    row_position = header['YFIELD']
    return (col_position, row_position)


def get_pixel_scale(fits_file):

    """
    Return the psf pixel scale
    Input: fits file
    Output: the psf pixel scale as a float

    """
    data, header = get_data_and_header(fits_file)
    pixel_scale = header['PIXSCALE']
    return pixel_scale


def get_psf_size(fits_file):

    """
    Return the psf size
    Input: fits file
    Output: the psf size as a tuple (col, row)

    """
    data, header = get_data_and_header(fits_file)
    pixel_width = header['NAXIS1']
    pixel_length = header['NAXIS2']
    return pixel_width, pixel_length


def get_ellipticity_fit(airy_fit):
    fwhm_x, fwhm_y = airy_fit.get_fwhm()
    module_e=math.fabs((fwhm_x**2-fwhm_y**2)/(fwhm_x**2+fwhm_y**2))
    return module_e


def get_fit_airy_parameter(data):

    """
    Return the fit parameters using an airy model of the image
    Input: psf image
    Output: Return the ellipticity and the circled energy at 65%
	    as a tuple (col, row)

    """
    try:
        airy_fit = psf_fitting.AiryPSFfit(data)
        ellipticity = get_ellipticity_fit(airy_fit)
        r65 = airy_fit.get_r65()
    except:
        ellipticity, r65 = np.NaN, np.NaN

    return ellipticity, r65


def get_apo_moment_parameter(data, pixscl, sigma=0.75, n_iter = 4):

    """
    Return the gaussian weighted second moments of the image
    Input: psf image
    Output: Return the ellipticity, r2 and the inclinason angle
	    as a tuple (col, row)

    """
    res = mk_ellipticity.get_moment_parameter(data, sigma, pixscl, n_iter)
    ellipticity = res['e']
    r2 = res['r2']
    angle = res['theta']
    return ellipticity, r2, angle


def mafonction(data_dir, result_file_name, gaussian_sigma = 0.75):

   psf_files = glob.glob(data_dir + '*.fits')
   result_file = open(result_file_name, 'w+')

   #print('#Results of the SC3 simulated VIS PSF')
   print('#pixel_scale pixel_width pixel_length col_position, row_position \
   ellipticity_moment R2_moment inclinaison_moment \
   ellipticity_fit r65_fit', file=result_file)

   for files in psf_files[0:10]:

       data, header = get_data_and_header(files)

       col_pos, row_pos = get_psf_position(files)
       pixel_scale = get_pixel_scale(files)
       psf_size = get_psf_size(files)

       ellipticity_mom, r2_mom, angle_mom = get_apo_moment_parameter(data, sigma=gaussian_sigma, pixscl=pixel_scale, n_iter = 4)

       ellipticity_fit, r65_fit = get_fit_airy_parameter(data)

       result_file.write(("%1.6f %i %i %1.6f %1.6f %1.6f %1.6f %1.6f %1.6f %1.6f %s") % 
			(pixel_scale, psf_size[0], psf_size[1], col_pos, row_pos, 
			ellipticity_mom, r2_mom, angle_mom,
			ellipticity_fit, r65_fit, "\n"))

   result_file.close

   return 0


if __name__ == "__main__":
	#print(sys.argv)
	parser = argparse.ArgumentParser()
	
	parser.add_argument("-i", "--input-file", dest="input", required=True, help="Path to the input file")	
	parser.add_argument("-o", "--output-file", dest="output", required=True, help="Path and name of the results file")
	parser.add_argument("-s", "--gaussian-sigma", dest="sigma", required=False, type=float, default=0.75, help="Width of the gaussian fit model")
	
	args = parser.parse_args()

	errcode = mafonction(args.input, args.output, args.sigma)
	
	sys.exit(errcode)


#sc3_data_dir = '/home/user/Work/data_sc3/psfs_effectives_sc3/'
#sc456_data_dir = '/home/user/Work/data_sc456/PSF_eff_sc456_SED_M/PSF_eff_sc456_SED_M_CCD00/'
#result_file_name = 'result_psf_effective_sc3.txt'


















