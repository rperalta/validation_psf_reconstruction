#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
=================
plot_psf_analysis_results.py
=================

desciption:
    Plot a parameter


usage: 
  plot_psf_analysis_result.py [-h] -f FILE-NAME -p PARAMETER
                                   [-c CHALLENGE]

Args:
  -f, --file-name       Path and name of the file results PSF parameter
  -p, --parameter       Parameter to plot: 
                        'e_mom': ellipticity deduced with the weighted second moment
                        'r2_mom': R2 deduced with the weighted second moment
                        'ang_mom': Inclinaison deduced with the weighted second moment
                        'e_fit': ellipticity deduced with a Airy fit
                        'r65_fit': circled energy r65 deduced with a Airy fit

Optionals:
  -c, challenge         Name of the challenge
  -h, --help            show this help message and exit

Example:
  python plot_psf_analysis_result.py -f result_sc3.txt -p e_mom -c SC3


Version:
  1.0

Author & date:
  R. PERALTA - Sept 2019 

"""

import argparse
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection


def plot_parameter(file_name, parameter, challenge=None):

    ## Choice param['value']eter : np.asarray(a, dtype=float)
    #parameter = 'e_mom'
    #challenge = 'SC3'
    #file_name = 'result_sc3.txt'

    #pixel_scale pixel_width pixel_length col_position, row_position    ellipticity_moment R2_moment inclinaison_moment    ellipticity_fit r65_fit
    pixel_scale, pixel_width, pixel_length, col, row, e_mom, r2_mom, ang_mom, e_fit, r65_fit = np.loadtxt(file_name, skiprows=1, unpack=True)


    param = {}
    if parameter == "e_mom":
        param['name'], param['method'], param['value'], param['unity'] = 'ellipticity', 'Weighted second moment', e_mom, '(no unity)'
    if parameter == "r2_mom":
        param['name'], param['method'], param['value'], param['unity'] = 'R2', 'Weighted second moment', r2_mom, '(no unity)'
    if parameter == "ang_mom": 
        param['name'], param['method'], param['value'], param['unity'] = 'Inclinaison', 'Weighted second moment', ang_mom, '[degree]'
    if parameter == "e_fit": 
        param['name'], param['method'], param['value'], param['unity'] = 'ellipticity', 'Airy fit', e_fit, '(no unity)'
    if parameter == "r65_fit": 
        param['name'], param['method'], param['value'], param['unity'] = 'circled energy r65', 'Airy fit', r65_fit, '[pixels]'


    # Square size in degree
    width_degree = pixel_width[0]*pixel_scale[0]/3600
    length_degree = pixel_length[0]*pixel_scale[0]/3600


    # Statistic
    if challenge!=None : print(("PSF from Challenge %s") % challenge)
    print("Number of PSF: %i" % len(param['value']))
    print(("Statistic: min= %1.6f, max= %1.6f, mean= %1.6f, median= %1.6f, std= %1.6f") % 
    (min(param['value']), max(param['value']), np.mean(param['value']), np.median(param['value']), np.std(param['value']) ))


    # Filter
    filter=np.where( (e_fit<0.8) & (e_fit>0.01))[0]
    param['value']=param['value'][filter]
    col=col[filter]
    row=row[filter]

    
    # Plots 
    plt.scatter(col,row, c=param['value'], cmap='jet', s=40, marker='s')
    plt.colorbar()
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    patches = [ plt.Rectangle((x, y), width_degree*10, length_degree*10) for x, y in zip(col-width_degree/2., row-length_degree/2.) ]
    
    collec = PatchCollection(patches, cmap='jet') #alpha=0.1
    
    collec.set_array(param['value'])
    ax.add_collection(collec)
    ax.set_xlim([min(col)-0.05, max(col)+0.05]) 
    ax.set_ylim([min(row)-0.05, max(row)+0.05]) 
    plt.colorbar(collec)
    
    plt.ylabel('VIS FPA height [degree]')
    plt.xlabel('VIS FPA width [degree]')
    if challenge!=None : 
        plt.title('VIS FPA - PSF %s - Parameter: %s %s %s Method: %s' % (challenge, param['name'], param['unity'], '\n', param['method']))
    else :
        plt.title('Parameter: %s %s %s Method: %s' % (param['name'], param['unity'], '\n', param['method']))
    
    plt.show()

    return 0


if __name__ == "__main__":

	parser = argparse.ArgumentParser()
	
	parser.add_argument("-f", "--file-name", dest="file", required=True, help="Path and name of the file results PSF parameter")
	parser.add_argument("-p", "--parameter", dest="parameter", required=True, help="Parameter to plot: 'e_mom': ellipticity deduced with the weighted second moment; 'r2_mom': R2 deduced with the weighted second moment; 'ang_mom': Inclinaison deduced with the weighted second moment; 'e_fit': ellipticity deduced with a Airy fit; 'r65_fit': 'circled energy r65 deduced with a Airy fit")
	parser.add_argument("-c", "--challenge", dest="challenge", required=False, help="Name of the challenge")	
	
	args = parser.parse_args()

	errcode = plot_parameter(args.file, args.parameter, args.challenge)
	
	sys.exit(errcode)





