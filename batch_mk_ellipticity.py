#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import sys
import os
import glob
import time
import astropy.io.fits as pyfits
import numpy as np
import matplotlib.pyplot as plt
import asciitable as at
sys.path.append(os.path.abspath("/home/rperalta/Documents/Euclid/psf/devellopement/psf_effective/pseff_vis/"))
from mk_ellipticity import *

cur_dir = os.getcwd()

data_dir = '/home/rperalta/Documents/Euclid/psf/devellopement/psf_effective/pseff_vis/'
#psffiles = glob.glob( data_dir + 'PSF_pdr_cool_69_v.1_NISP_NIS-blue_1024x1024_900nm_*.fits')
psffiles = glob.glob( data_dir + 'PSF_WFM_40x40_LFobs*.fits')
print(psffiles)

#psffiles = glob.glob("Huygens_pdr_cool_69_v.1_minimal_pos.cat_000*_*00nm_pxsz_1.0um.fits")
#psffiles = glob.glob("PSF_pdr_cool_69_v.1_proper_1024x1024_1000nm_Grid_801x706_*.fits")

f1=open('./Huygens_shapes_Pierre.log', 'w+')
for files in psffiles:
	image = pyfits.getdata(files)
	res = get_ellipticity(image,
                          sigma=0.75,
                          pixscl=0.0083333,
                          n_iter=4)
	text = ("{fil}, {mu_x:1.4f}, {mu_y:1.4f}, {q_xx:1.4f}, {q_yy:1.4f}, {q_xy:1.4f}, "
			"{e1:1.6f}, {e2:1.6f}, {r2:1.6f}")
	print(text.format(fil=files, **res))
	print('#PSF_FILE mu_x mu_y q_xx q_yy q_xy e1 e2 r2', file=f1)
	print(text.format(fil=files, **res), file=f1)

#resa = np.array.append([get_field_pos(f) for f in psffiles])

#psfprop=at.read('Huygens_shapes_Pierre.log')
#np.std(psfprop.e1)



